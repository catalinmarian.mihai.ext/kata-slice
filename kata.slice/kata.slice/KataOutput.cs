﻿namespace kata.slice
{
    public record KataOutput(decimal Price, string Currency);
}
