﻿namespace kata.slice
{
    internal class KataInput
    {
        public int ReamsCount { get; set; }
        public string CountryCode { get; set; }
        public decimal UnitaryPrice { get; set; }

        public KataInput(int reamsCount, string countryCode, decimal unitaryPrice)
        {
            ReamsCount = reamsCount;
            CountryCode = countryCode;
            UnitaryPrice = unitaryPrice;
        }
    }


    internal static class KataValidator
    {
        internal const string supportedStates = "UT NV TX AL CA";
        internal static bool ValidateReamsCount(int reamsCount)
            => reamsCount < 0;

        internal static bool ValidateCountryCode(string countryCode)
            => supportedStates.Contains(countryCode);

        internal static bool ValidateUnitaryPrice(decimal unitaryPrice)
            => unitaryPrice < 0;
    }
}
