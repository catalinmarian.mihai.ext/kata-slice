﻿// See https://aka.ms/new-console-template for more information
using kata.slice;

Console.WriteLine("Hello, Thomas and welcome to AMERICAAA!");
System.Threading.Thread.Sleep(1000);
Console.WriteLine("Please input the following:");
System.Threading.Thread.Sleep(1000);
Console.WriteLine("Number of reams:");
var reamsCount = int.Parse(Console.ReadLine());
while (KataValidator.ValidateReamsCount(reamsCount))
{
    Console.WriteLine("The provided number is not relevant, please intput it again.");
    System.Threading.Thread.Sleep(1000);
    reamsCount = int.Parse(Console.ReadLine());
}
Console.WriteLine("The price for ream unit:");
var unitPrice = decimal.Parse(Console.ReadLine());
while (KataValidator.ValidateUnitaryPrice(unitPrice))
{
    Console.WriteLine("The provided number is not relevant, please intput it again.");
    System.Threading.Thread.Sleep(1000);
    unitPrice = decimal.Parse(Console.ReadLine());
}
Console.WriteLine("The state you want to sell:");
var stateCode = Console.ReadLine();
while (!KataValidator.ValidateCountryCode(stateCode))
{
    Console.WriteLine("The provided state is not relevant, we're not selling there.");
    System.Threading.Thread.Sleep(1000);
    stateCode = Console.ReadLine();
}

Console.WriteLine("Wait a second, we're doing our pepperish job....");
var computationEngine = new ComputationEngine();
var output = computationEngine.Compute(new(reamsCount, stateCode, unitPrice));
Console.WriteLine($"{output.Price} {output.Currency}");
System.Threading.Thread.Sleep(4000);
Console.WriteLine("See you next demo!");