﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kata.slice
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void Basic_Display()
        {
            var input = new KataInput(700, "CA", 10);
            var computationEngine = new ComputationEngine();
            var output = computationEngine.Compute(input);
            Assert.AreEqual(7096, output);
        }
    }
}
