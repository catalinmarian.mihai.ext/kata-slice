﻿namespace kata.slice
{
    internal class ComputationEngine
    {
        public KataOutput Compute(KataInput inputs)
        {
            var priceWithoutVat = this.ComputePriceWithoutVat(inputs);
            var discountRate = this.ComputeDiscount(priceWithoutVat, inputs);
            var vatRate = this.CompteVatRate(inputs.CountryCode);

            var discountPrice = priceWithoutVat - (priceWithoutVat * discountRate / 100);
            var vatRatePrice = discountPrice * vatRate / 100;
            var finalPrice = discountPrice + vatRatePrice;
            KataOutput output = new(finalPrice, "$");
            return output;
        }

        private decimal CompteVatRate(string countryCode)
        {
            var sanitizedCode = countryCode.ToLowerInvariant();
            switch (sanitizedCode)
            {
                case "UT":
                    return 6.85m;

                case "NV":
                    return 8;

                case "TX":
                    return 6.25m;

                case "AL":
                    return 4;

                case "CA":
                    return 8.25m;

                default:
                    throw new InvalidOperationException("Invalid tax rate");
            }
        }

        private decimal ComputePriceWithoutVat(KataInput inputs)
            => inputs.ReamsCount * inputs.UnitaryPrice;

        private decimal ComputeDiscount(decimal priceWithoutVat, KataInput inputs)
        {
            if (priceWithoutVat < 1000)
                return 0;

            if (priceWithoutVat >= 1000 && priceWithoutVat < 5000)
                return 3;

            if (priceWithoutVat >= 5000 && priceWithoutVat < 7000)
                return 5;

            if (priceWithoutVat >= 7000 && priceWithoutVat < 10000)
                return 7;

            if (priceWithoutVat >= 10000 && priceWithoutVat < 50000)
                return 10;

            if (priceWithoutVat >= 50000)
                return 15;

            throw new InvalidOperationException("Bad discount rate");
        }
    }
}